cd /Server
git pull
git submodule update --recursive --remote
cd $GOPATH/src/github.com/thorstenkloehn/ahrensburg-digital
git pull
go build
sudo systemctl stop ahrensburg.service
sudo systemctl start ahrensburg.service

cd /Server/node
npm install

sudo service supervisor restart


dotnet publish --configuration Release
sudo systemctl stop asp.service
sudo systemctl start asp.service