sudo apt update -y
sudo apt upgrade -y
sudo apt-get install supervisor -y
sudo apt-get install git -y
sudo apt-get install git-core -y
sudo apt-get install curl -y
sudo apt-get install cmake gcc clang gdb build-essential git-core -y
sudo apt-get install postgresql postgresql-client postgis    -y
sudo git submodule update --init --recursive
sudo git submodule update --recursive --remote
go get github.com/thorstenkloehn/ahrensburg-digital
cd $GOPATH/src/github.com/thorstenkloehn/ahrensburg-digital
go build
cd /Server
sudo cp -u ahrensburg.service /etc/systemd/system/ahrensburg.service
sudo  systemctl enable ahrensburg.service
sudo systemctl start ahrensburg.service
sudo apt-get install python3  python3-pip -y
sudo pip3 install flask
sudo apt-get install gunicorn -y
sudo cp -u python.conf /etc/supervisor/conf.d/python.conf
sudo supervisorctl reread
sudo service supervisor restart
cd /Server/node
npm install
cd /Server
sudo cp -u node.conf /etc/supervisor/conf.d/node.conf
